//
//  MyPostsModel.swift
//  Instafeed
//
//  Created by Eric on 2019/10/16.
//  Copyright © 2019 backstage supporters. All rights reserved.
//

import Foundation

class MyPostsModel {
    
    var id: String?
    private var url: String
    private var title: String
    private var date: String
    var image_360x290:String
    var video_thumb:String
    var dt_added:String?
    var avatar:String?
    var username:String?
    var user_id:String?
    var is_anonymous:String?
    
    init(image_360x290: String = "",video_thumb: String = "",dt_added:String = "",avatar:String = "",username:String = "",user_id:String = "",is_anonymous:String = "") {
        self.id = ""
        self.url = ""
        self.title = ""
        self.date = ""
        self.dt_added = dt_added
        self.avatar = avatar
        self.image_360x290 = image_360x290
        self.video_thumb = video_thumb
        self.username = username
        self.user_id = user_id
        self.is_anonymous = is_anonymous
    }
    
    init(id: String, url: String, title: String, date: String, image_360x290: String,video_thumb: String,dt_added:String?,avatar:String?,username:String?,user_id:String?,is_anonymous:String? ) {
        self.id = id
        self.url = url
        self.title = title
        self.date = date
        self.image_360x290 = image_360x290
        self.video_thumb = video_thumb
        self.dt_added = dt_added
        self.avatar = avatar
        self.username = username
        self.user_id = user_id
        self.is_anonymous = is_anonymous
    }
    
    public func getId() -> String {
        return id ?? ""
    }
    
    public func getUrl() -> String {
        return url
    }
    
    public func getTitle() -> String {
        return title
    }
    
    public func getDate() -> String {
        return date
    }
    public func get_dt_added() -> String {
        return dt_added ?? ""
    }
    public func getAvtar() -> String {
        return avatar ?? ""
    }
    public func getUsername() -> String {
        return username ?? ""
    }
    public func get_user_id() -> String {
        return user_id ?? ""
    }
    public func get_is_anonymous() -> String {
        return is_anonymous ?? ""
    }
}
