//
//  LikesVC.swift
//  Instafeed
//
//  Created by HKC on 10/19/19.
//  Copyright © 2019 backstage supporters. All rights reserved.
//

import UIKit

class LikesVC: UIViewController {

    var postID: String!
    var feedType: FeedType = .None
    var likeList = [likeListdata]()
    var feed = feedDatadata()
    var moduleType = String()
    var userName : String?

    @IBOutlet weak var likesTVC: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        likesTVC.register(UINib(nibName: "LikeTableViewCell", bundle: nil), forCellReuseIdentifier: "LikeTableViewCell")
        getNews(postId: self.postID)

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        getLikes()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func getNews(postId: String) {
        var url = String()
           if moduleType == "1" {
               url = "citizen/posts"
           } else if moduleType == "2" {
               url = "star/posts"
           } else if moduleType == "3" {
               url = "brands/posts"
           } else if moduleType == "4" {
               url = "users/posts"
           } else {
               url = "brands/posts"
           }
        let apiurl = ServerURL.firstpoint + url
        let params = ["id":postId] as [String:Any]
       
        networking.MakeRequest(Url: apiurl, Param: params, vc: self) { (response:feedDescription) in
            print(response)
            
            if response.message == "error"{
                Toast().showToast(message: "Something went wrong please try again later!!!", duration: 2)
                self.navigationController?.popViewController(animated: true)
            } else {
                if let data = response.data {
                    print(data as Any)
                    self.feed = data[0]
                }
            }
        }
    }
    
    
    
    fileprivate func getLikes(){
        var apiURL: String = ""
        switch feedType {
        case .Citizen:
            apiURL = ServerURL.firstpoint + ServerURL.citizenLikesURL
            break
        case .Brand:
            apiURL = ServerURL.firstpoint + ServerURL.brandLikesURL
            break
        case .Star:
            apiURL = ServerURL.firstpoint + ServerURL.starLikesURL
            break
        default:
            break
        }
        
        guard let UserToken = UserDefaults.standard.value(forKey: "SavedToken") as? String else {return}
        let params = ["token":UserToken, "id":postID!] as [String:Any]
        
        networking.MakeRequest(Url: apiURL, Param: params, vc: self) { (result:likeList) in
            print(result.message)
            
            if result.message == "success" {
                if let response = result.data {
                    print(response)
                    self.likeList = response.map{$0}
                    print("your array -> \(self.likeList)")
                    DispatchQueue.main.async {
                        self.likesTVC.reloadData()
                    }
                }
            }else{
                CommonFuncs.AlertWithOK(msg: Alertmsg.wentwrong, vc: self)
                return
            }
        }
    }
    

    @IBAction func actionBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}


extension LikesVC : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return likeList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LikeTableViewCell") as! LikeTableViewCell
        cell.like = likeList[indexPath.row]
        if likeList[indexPath.row].follow_status == "0"{
            cell.buttonFollow.setTitle("FOLLOW", for: .normal)
            cell.buttonFollow.setTitleColor(UIColor(hexString: "FF6600"), for: .normal)
            cell.buttonFollow.backgroundColor = .white
        }else{
            cell.buttonFollow.setTitle("FOLLOWING", for: .normal)
            cell.buttonFollow.setTitleColor(.white, for: .normal)
            cell.buttonFollow.backgroundColor = UIColor(hexString: "FF6600")
        }
        cell.cellIndexPath = indexPath
        cell.delegate = self
       
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return tableView.frame.size.height / 10.0
    }
}
extension LikesVC: LikeTableViewCellDelegate{
    func userHasClickedFollowButton(index: IndexPath) {
        let cell = likesTVC.cellForRow(at: index) as! LikeTableViewCell
        guard let UserToken = UserDefaults.standard.value(forKey: "SavedToken") as? String else {return}
        guard let userName = self.likeList[index.row].username  else{return}
        guard let id = self.likeList[index.row].avatar_userid else{return}
        CommonFuncs.addfollow(url: self.likeList[index.row].follow_status == "0" ? ServerURL.addfollow :ServerURL.unfollow ,username: userName,vc: self, postid: id, token: UserToken, moduleId: self.moduleType, completionHandler: {resp, err in
            if resp?.message == "success"{
                if self.likeList[index.row].follow_status == "0"{
                    self.likeList[index.row].follow_status = "1"
                    cell.buttonFollow.setTitle("FOLLOWING", for: .normal)
                    cell.buttonFollow.setTitleColor(.white, for: .normal)
                    cell.buttonFollow.backgroundColor = UIColor(hexString: "FF6600")
                }else{
                    self.likeList[index.row].follow_status = "0"
                    cell.buttonFollow.setTitle("FOLLOW", for: .normal)
                    cell.buttonFollow.setTitleColor(UIColor(hexString: "FF6600"), for: .normal)
                    cell.buttonFollow.backgroundColor = .white
                }
            }
        })
    }
}
