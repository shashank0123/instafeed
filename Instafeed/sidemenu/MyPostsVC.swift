//
//  MyPostsVC.swift
//  Instafeed
//
//  Created by Eric on 2019/10/16.
//  Copyright © 2019 backstage supporters. All rights reserved.
//

import UIKit
import Alamofire
import ProgressHUD

class MyPostsVC: UIViewController {

    @IBOutlet weak var tblview: UITableView!
    var postList = [MyPostsModel]()
    
    var searchTextBefore:String? = nil
    var searchType:String? = nil
    var myID : String = ""
    
    @IBOutlet var lbTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tblview.delegate = self
        tblview.dataSource = self
        if searchTextBefore == nil{
            reloadTableView()
        }else{
            getTagData()
            getMyProfileData()
            self.lbTitle.isHidden = true
        }
    }
    
    @IBAction func backFromMyPosts(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @objc func editPost(sender: UIButton) {
        
        var typeValue = "0"
        if let type = UserDefaults.standard.value(forKey: "UserType"){
            typeValue = "\(type)"
        }

        let UserType = Int(typeValue)!

        if UserType > 2 {
            var apiMethod = "3"
            if UserType == 4 {
                apiMethod = "1"
            } else if UserType == 5 {
                apiMethod = "2"
            }
            
            constnt.isEditPost = true
            let storyboard = UIStoryboard(name: "Feeds", bundle: nil)
            let mainfeeds = storyboard.instantiateViewController(withIdentifier: "Mainfeeds") as! Mainfeeds
            mainfeeds.moduleType = apiMethod
            mainfeeds.postID = "\(sender.tag)"
            self.present(mainfeeds, animated: true, completion: nil)
        }
    }

    
    @objc func deletePost(sender: UIButton) {
        
        var typeValue = "0"
        
        if let type = UserDefaults.standard.value(forKey: "UserType"){
            typeValue = "\(type)"
        }

        let UserType = Int(typeValue)!
//        let UserType = type! as! Int
        if UserType > 2 {
            var apiMethod = "brand"
            if UserType == 4 {
                apiMethod = "citizen"
            } else if UserType == 5 {
                apiMethod = "star"
            }
            let url: String = "\(ServerURL.firstpoint)\(apiMethod)/post/delete"
            guard let UserToken = UserDefaults.standard.value(forKey: "SavedToken") as? String else {return}
            let params : Parameters = ["token": UserToken, "id": sender.tag]
            ProgressHUD.show()
            Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.default).validate().responseJSON{ response in
                
                ProgressHUD.dismiss()
                
                switch response.result {
                case .success:
                    self.reloadTableView()
                case .failure(let error):
                    print("failed to load feeddata: \(error.localizedDescription)")
                }
            }
        }
    }
    
    func reloadTableView() {
        print("Data => \(UserDefaults.standard.value(forKey: "UserType") ?? "Blank")")
        let UserType:Int = UserDefaults.standard.integer(forKey: "UserType")
        if UserType > 2 {
            var apiMethod = "brand"
            if UserType == 4 {
                apiMethod = "news"
            } else if UserType == 5 {
                apiMethod = "star"
            }
            let url: String = "\(ServerURL.firstpoint)profile/\(apiMethod)"
            guard let UserToken = UserDefaults.standard.value(forKey: "SavedToken") as? String else {return}
            let params : Parameters = ["token": UserToken]
            self.postList.removeAll()
            Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.default).validate().responseJSON{ response in
                switch response.result {
                case .success:
                    
                    self.postList.removeAll()
                    
                    print("\(response)")
                    
                    let json_data = response.result.value as! [String: Any]
                    let data = json_data["data"] as! [[String: Any]]
                    for entry in data {
//                        let aResult = MyPostsModel(id: Int(entry["id"] as! String)!, url: entry["image"] as! String, title: entry["title"] as! String, date: entry["dt_modified"] as! String, image_360x290: entry["image"] as! String, video_thumb: entry["video_thumb"] as! String)
                     let aResult = MyPostsModel(id: entry["id"] as? String ?? "", url: entry["image"] as? String ?? "", title: entry["title"] as? String ?? "", date: entry["dt_modified"] as? String ?? "", image_360x290: entry["image"] as? String ?? "", video_thumb: entry["video_thumb"] as? String ?? "", dt_added: entry["dt_added"] as? String ?? "", avatar: entry["avatar"] as? String ?? "", username: entry["username"] as? String ?? "", user_id: entry["user_id"] as? String ?? "", is_anonymous: entry["is_anonymous"] as? String ?? "")
                        
                        self.postList.append(aResult)
                    }
                    self.tblview.reloadData()
                case .failure(let error):
                    print("failed to load feeddata: \(error.localizedDescription)")
                }
            }
        }
    }
    
    func getTagData() {
        var url: String = "\(ServerURL.firstpoint)\(searchType!)/hashtags"
        if searchType == "superstar" {
            url = "\(ServerURL.firstpoint)star/hashtags"
        }
        let params : Parameters = ["tag": "#" + searchTextBefore!]
        self.postList.removeAll()
        Alamofire.request(url, method: .post, parameters: params, encoding: URLEncoding.default).validate().responseJSON{ response in
            print(response.result.value!)
            
            switch response.result {
            case .success:
                let json_data = response.result.value as! [String: Any]
                let data = json_data["data"] as! [[String: Any]]
                for entry in data {
//                     MyPostsModel(id: Int(entry["id"] as! String)!, url: entry["image"] as! String, title: entry["title"] as! String, date: entry["dt_modified"] as! String, image_360x290: entry["image_360x290"] as! String, video_thumb: entry["video_thumb"] as! String)
                  let aResult =  MyPostsModel(id: entry["id"] as? String ?? "", url: entry["image"] as? String ?? "", title: entry["title"] as? String ?? "", date: entry["dt_modified"] as? String ?? "", image_360x290: entry["image"] as? String ?? "", video_thumb: entry["video_thumb"] as? String ?? "", dt_added: entry["dt_added"] as? String ?? "", avatar: entry["avatar"] as? String ?? "", username: entry["username"] as? String ?? "", user_id: entry["user_id"] as? String ?? "", is_anonymous: entry["is_anonymous"] as? String ?? "")
                    
                    self.postList.append(aResult)
                }
                self.tblview.reloadData()
            case .failure(let error):
                print("failed to load feeddata: \(error.localizedDescription)")
            }
        }
    }
    
    
    //MARK : GET PROFILE DATA
    
    
    func getMyProfileData(){
        ProfileWorker().getProfileData { (Data, err) in
            if err == nil{
                print(Data?.user_id)
                if Data?.user_id != ""{
                    self.myID = (Data?.user_id)!
                    self.tblview.reloadData()
                }
            }else{
                CommonFuncs.AlertWithOK(msg: (err?.message)!, vc: self)
            }
        }
    }
    
}

extension MyPostsVC : UITableViewDelegate, UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MyPostsCellID") as! MyPostsCell
        
//        let imgUrl = URL(string: postList[indexPath.row].getUrl())
//        let data = try? Data(contentsOf: imgUrl!)
//        if let imageData = data {
//            cell.postImage.image = UIImage(data: imageData)
//        }
        
        if self.myID == postList[indexPath.row].user_id{
            cell.editButton.isHidden = false
        }else{
            cell.editButton.isHidden = true
        }
        cell.postImage.sd_setImage(with: URL(string: postList[indexPath.row].image_360x290.contains("default") ?  postList[indexPath.row].video_thumb : postList[indexPath.row].image_360x290), placeholderImage: UIImage(named: "citizelcell"), options: .highPriority, completed: nil)
        cell.titleLabel.text = postList[indexPath.row].getTitle()
        cell.dateLabel.textColor = UIColor.lightGray
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let dateInFormat = dateFormatter.date(from: postList[indexPath.row].getDate())
        dateFormatter.dateFormat = "dd MMMM yyyy"
        cell.dateLabel.text = dateFormatter.string(from: dateInFormat!)
        cell.editButton.tag = Int(postList[indexPath.row].getId()) ?? 0
        cell.delButton.tag = Int(postList[indexPath.row].getId()) ?? 0
        cell.editButton.addTarget(self, action: #selector(editPost), for: .touchUpInside)
        cell.delButton.addTarget(self, action: #selector(deletePost), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 112.0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let storyBoard = UIStoryboard(name: "categoryStoryboard", bundle: nil)
        if let vc = storyBoard.instantiateViewController(withIdentifier: "Articlescreen") as? Articlescreen {
            var typeValue = "0"
            var apiMethod = "3"
            if let type = UserDefaults.standard.value(forKey: "UserType"){
                typeValue = "\(type)"
            }
            let UserType = Int(typeValue)!
            if UserType > 2 {
                    apiMethod = "3"
                if UserType == 4 {
                    apiMethod = "1"
                } else if UserType == 5 {
                    apiMethod = "2"
                }
            }
            if tableView == tblview{
                vc.moduleType = apiMethod
                
                vc.dt_added = self.postList[indexPath.row].dt_added ?? ""
//                vc.userAvtar = self.postList[indexPath.row].avatar
                vc.myPostDetail = self.postList[indexPath.row]
                if let postId = self.postList[indexPath.row].id{
                    vc.postId = postId
                }
            

                self.navigationController?.pushViewController(vc, animated: true)
            }
        }
    }
}
